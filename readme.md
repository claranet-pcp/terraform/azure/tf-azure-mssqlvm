# tf-azure-mssqlvm

Creates a single MSSQL VM in Azure.
Creates 5 additional drives, APP, DB, Log, TempDB, Index

## Usage
```
module "db" {
  source             = "../localmodules//tf-azure-mssqlvm" #module location
  customer            = "example" 
  envtype            = "non prod"
  azure_location     = northeurope"
  azure_location_short = "ne"
  local_adminuser          = "local admin"
  local_adminpassword        = "password"
  nat_subnets                = "10.0.20.0"
 instance_number = "2" #number of instances to create
  sql_adminuser = "sqladmin"
  sql_adminpassword = "sqlpassword"

#domain settinga
  ad_domain = "example.com"
  domainadmin_username = "domainadmin"
  domainadmin_password = "domain password"
}

```



## Output
```
 -- NONE --
 ```


 ## Notes
 ```
 *Requires Network module to be in place
 *Requires Azure DNS to be pointed at AD server for domain join
 ```

#Optional variables

```
# Server / SQL Version
variable "offer" {
  default = "SQL2014SP2-WS2012R2" 
}

# SQL Edition (web / standard / enterprise)
variable "sku" {
  default = "Enterprise"
}

variable "app_disk_size" {
  default = "128"
}

variable "data_disk_size" {
  default = "128"
}

variable "log_disk_size" {
  default = "128"
}

variable "tempdb_disk_size" {
  default = "128"
}

variable "dbindex_disk_size" {
  default = "128"
}

```